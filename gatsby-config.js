const path = require('path');

module.exports = {
  siteMetadata: {
    siteUrl: `https://bachem.pl`
  },
  plugins: [
    'gatsby-plugin-advanced-sitemap',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-styled-components',
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: path.join(__dirname, 'src', 'images')
      }
    },
    'gatsby-transformer-json',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'data',
        path: path.join(__dirname, 'src', 'database')
      }
    },

    {
      resolve: 'gatsby-plugin-typescript',
      options: {
        isTSX: true, // defaults to false
        allExtensions: true // defaults to false
      }
    },
    {
      resolve: 'gatsby-plugin-anchor-links',
      options: {
        offset: -90,
        duration: 1
      }
    },
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        trackingIds: ['G-WFWFYH3L89'],
        gtagConfig: {
          // optimize_id: 'OPT_CONTAINER_ID',
          anonymize_ip: true,
          cookie_expires: 0
        },
        pluginConfig: {
          head: false,
          respectDNT: true
        }
      }
    },
    {
      resolve: 'gatsby-plugin-htaccess',
      options: {
        https: true,
        www: false
      }
    },
    {
      resolve: 'gatsby-plugin-zopfli'
    }
  ]
};
