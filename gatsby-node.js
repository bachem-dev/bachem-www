/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
const path = require('path');
const categories = require('./src/database/offer.json');
const products = require('./src/database/products.json');

exports.onCreateWebpackConfig = ({ actions, stage }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, 'src'), 'node_modules']
    }
  });

  if (stage === 'build-html') {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /mapbox-gl/,
            use: ['null-loader']
          }
        ]
      }
    });
  }
};

exports.createPages = ({ actions }) => {
  const { createPage } = actions;
  const categoryTemplate = path.resolve('./src/templates/category.tsx');
  const productTemplate = path.resolve('./src/templates/product.tsx');

  products.forEach((prod) => {
    const categoriesOfProduct = categories.filter((cat) => prod.categories.includes(cat.slug));
    const path = `oferta/${categoriesOfProduct[0].slug}/${prod.slug}/`;

    createPage({
      path,
      component: productTemplate,
      context: { categories: categoriesOfProduct, product: prod }
    });
  });

  categories.forEach((cat) => {
    const path = `oferta/${cat.slug}/`;
    const productsInCategories = products.filter((prod) => prod.categories.includes(cat.slug));

    createPage({
      path,
      component: categoryTemplate,
      context: { category: cat, products: productsInCategories }
    });
  });
};
