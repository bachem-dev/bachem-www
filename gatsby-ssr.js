import React from 'react';
import { theme } from './src/lib/theme';

import { ThemeProvider } from 'styled-components';

export const wrapRootElement = ({ element }) => {
  return <ThemeProvider theme={theme}>{element}</ThemeProvider>;
};
