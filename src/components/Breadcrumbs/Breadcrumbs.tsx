import React from 'react';

import { browserWindow } from 'lib/utils';

import { BreadcrumbsElements } from './elements';

const { Container, Crumb } = BreadcrumbsElements;

interface Crumb {
  link: string;
  display: string;
}

export const Breadcrumbs: React.FC<{ crumbs: Crumb[] }> = ({ crumbs }) => {
  const { pathname } = browserWindow().location;

  return (
    <Container>
      <Crumb to="/" active={pathname === '/'}>
        Strona główna
      </Crumb>
      {crumbs.map((crumb) => (
        <Crumb key={crumb.link} to={crumb.link} active={pathname === crumb.link}>
          {crumb.display}
        </Crumb>
      ))}
    </Container>
  );
};
