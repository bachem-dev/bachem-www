import React, { useState } from 'react';
import { PoseGroup } from 'react-pose';

import { VideoOverlay, Player, VideoWrapper, PlayButton } from './elements';
import playIcon from './player-icon.svg';
import * as video from './sample-vid.mp4';

export const Video: React.FC<{ className?: string }> = ({ className = '' }) => {
  const [isPlaying, setIsPlaying] = useState(false);

  return (
    <VideoWrapper className={className}>
      <PoseGroup>
        {!isPlaying && (
          <VideoOverlay key="vidOverlay" onClick={() => setIsPlaying(true)}>
            <PlayButton src={playIcon} alt="Play Button" />
          </VideoOverlay>
        )}
      </PoseGroup>
      <Player
        width="100%"
        height="100%"
        url={video}
        controls={isPlaying}
        playing={isPlaying}
        onPause={() => setIsPlaying(false)}
      />
    </VideoWrapper>
  );
};
