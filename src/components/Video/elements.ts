import ReactPlayer from 'react-player';
import posed from 'react-pose';
import styled from 'styled-components';

export const VideoWrapper = styled.div`
  /* providing aspect ratio box */
  padding: 28.125% 0;
  position: relative;
`;

export const Player = styled(ReactPlayer)`
  display: block;
  left: 0;
  position: absolute;
  top: 0;
  z-index: 1;
`;

const PoseComp = posed.div({
  enter: { opacity: 1 },
  exit: { opacity: 0 }
});

export const VideoOverlay = styled(PoseComp)`
  align-items: center;
  background-color: ${({ theme }) => theme.color.blue[400]}99;
  bottom: 0;
  cursor: pointer;
  display: flex;
  justify-content: center;
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 2;
`;

export const PlayButton = styled.img`
  height: 130px;

  ${/* sc-selector */ VideoOverlay}:hover > & {
    opacity: 0.8;
  }
`;
