export namespace Offer {
  export interface Data {
    id: number;
    title: string;
    description: string;
    shortDescription: string;
    slug: string;
    isMain: boolean;
    type: string;
  }
}
