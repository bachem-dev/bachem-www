import React from 'react';

import { HeadingElements } from './elements';

const { Container, Title, Description } = HeadingElements;

interface HeadingComposition {
  Title: React.FC;
  Description: React.FC;
}

export const Heading: React.FC & HeadingComposition = ({ children }) => <Container>{children}</Container>;

Heading.Title = Title;
Heading.Description = Description;
