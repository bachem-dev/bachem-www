import styled, { css } from 'styled-components';

const Container = styled.div(
  ({ theme }) => css`
    align-items: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-bottom: ${theme.spacing.big};
    width: 100%;

    ${theme.onScreen('TABLET_LANDSCAPE')} {
      margin: 40px 0;
    }

    ${theme.onScreen('MEDIUM_DESKTOP')} {
      margin: 80px 0;
    }
  `
);

const Title = styled.h2(
  ({ theme }) => css`
    ${theme.font(26, 'SEMIBOLD')};
    color: ${theme.color.blue[400]};

    ${theme.onScreen('TABLET_LANDSCAPE')} {
      ${theme.font(32, 'BOLD')};
    }
  `
);

const Description = styled.h3(
  ({ theme }) => css`
    color: ${theme.color.gray[200]};
    text-align: center;

    ${theme.onScreen('TABLET')} {
      max-width: 60vw;
    }

    ${theme.onScreen('TABLET_LANDSCAPE')} {
      max-width: 800px;
      ${theme.font(20)};
    }
  `
);

export const HeadingElements = {
  Container,
  Title,
  Description
};
