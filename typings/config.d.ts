interface Config {
  meta: {
    description: string;
    keywords: string;
  };
  mapOptions: {
    style: string;
    center: Record<string, number[]>;
    marker: Record<string, number[]>;
  };
  routePoints: number[][];
  routeLink: Record<string, string>;
}
